FROM mono:4

LABEL maintainer "J.W.Marsden <j.w.marsden@gmail.com>"

ENV TERRARIA_VERSION 1353
ENV TERRARIA_WORLD_NAME world
ENV TERRARIA_MOTD Please dont cut down the trees!
ENV TERRARIA_MAX_PLAYERS 8
ENV TERRARIA_PASSWORD terraria
ENV TERRARIA_AUTOCREATE 1
ENV TERRARIA_DIFFICULTY 0
ENV TERRARIA_LANGUAGE en/US

RUN apt-get update -y &&\
	apt-get install -y unzip wget
RUN wget -q -O terraria-server.zip http://terraria.org/server/terraria-server-${TERRARIA_VERSION}.zip &&\
	unzip terraria-server.zip -d terraria &&\
	rm terraria-server.zip

RUN mkdir -p /terraria/conf
ADD start.sh /terraria/start.sh
RUN chmod +x /terraria/start.sh

# Exposes the volume so it can be linked externally
VOLUME ["/terraria/conf"]

WORKDIR /terraria

EXPOSE 7777

# Encapsulate the logic for starting in a script
# ENTRYPOINT ["/terraria/start.sh"]