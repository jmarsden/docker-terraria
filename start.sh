#!/bin/bash

exit_handler() {
    echo "Shutdown signal received"
    echo "Exiting via shutdown signal . . ."
    exit
}
trap 'exit_handler' SIGHUP SIGINT SIGQUIT SIGTERM

echo "Starting Server."

TERRARIA_PATH="/terraria"
TERRARIA_BIN="${TERRARIA_PATH}/${TERRARIA_VERSION}/Linux/TerrariaServer.exe"
TERRARIA_CONF="${TERRARIA_PATH}/conf"
TERRARIA_CONF_FILE="${TERRARIA_CONF}/config.txt"
TERRARIA_BANLIST_FILE="${TERRARIA_CONF}/banlist.txt"
TERRARIA_WORLD="${TERRARIA_CONF}/${TERRARIA_WORLD_NAME}.wld"

if [ -f ${TERRARIA_CONF_FILE} ]; then
    OPTS="-config ${TERRARIA_CONF_FILE}"
fi

if [ ! -f ${TERRARIA_CONF_FILE} ]; then
	OPTS="-players ${TERRARIA_MAX_PLAYERS}"
#	OPTS="${OPTS} -pass ${TERRARIA_PASSWORD}"
#	OPTS="${OPTS} -motd \"${TERRARIA_MOTD}\""
	OPTS="${OPTS} -world ${TERRARIA_WORLD}"
	OPTS="${OPTS} -autocreate ${TERRARIA_AUTOCREATE}"
	OPTS="${OPTS} -difficulty ${TERRARIA_DIFFICULTY}"
#	OPTS="${OPTS} -worldname ${TERRARIA_WORLD_NAME}"
fi

if [ -f ${TERRARIA_BANLIST_FILE} ]; then
    OPTS="${OPTS} -banlist ${TERRARIA_BANLIST_FILE}"
fi

echo "mono ${TERRARIA_BIN} ${OPTS}"

exec mono ${TERRARIA_BIN} ${OPTS}